# Comandos Linux

## Versión de Linux

```
uname -a
```
```
lsb_release -a
```
## Permiso en Linux

* (-) Archivo.
* (d) Directorio.
* (r) read.
* (w) write.
* (x) execute.

|Permisos|Pertenece|
|--------|---------|
|rwx --- ---|Usuario|
|--- r-x ---|Grupo|
|--- --- r-x|Otros|


### Chmod en Octal


|Octal|Lectura|Escritura|Ejecución|
|------|------|---------|---------|
|0|-|-|-|
|1|-|-|x|
|2|-|w|-|
|3|-|w|x|
|4|r|-|-|
|5|r|-|x|
|6|r|w|-|
|7|r|w|x|

### Gestión de permisos

Solo se puede cambiar los permisos de un archivo o directorio si se es propietario o el superusuario root. Por lo que se utiliza el comando ***chmod*** con la siguiente sintaxis:

~~~
$ chmod {a,u,g,o} {+,-} {r,w,x} fichero
~~~

* {a} all, Todos los usuarios.
* {u} user, Propietario.
* {g} group, Usuarios del grupo propietario.
* {o} other, Resto de los usurios del sistema.

### Ejemplos

El siguiente ejemplo da permisos de lectura todos los usuarios

~~~
$ chmod a+r fichero
~~~

La siguiente linea anula los permisos de ejecución a todos menos al usuario.

~~~
$ chmod og-x fichero
~~~

La siguiente linea de comando da permisos de lectura, escritura y ejecución al usuario.

~~~
$ chmod u+rwx fichero
~~~

## Comando GREP

Este comando toma una expresión regular de la linea de comando, lee la entrada estándar o una lista de archivos e imprime las lineas que tienen coincidencias.

La siguiente linea de comando muestra la cadena (que también se puede mostrar entre comillas) que contiene un archivo especifico.

~~~
$ grep "tal" nombre_archivo
~~~


La siguiente linea de comando busca la cadena de ***tal*** en el contenido de una lista de archivos.

~~~
$ grep tal *
~~~

Con el siguiente comando se puede buscar una cadena en una lista de archivos, como también dentro de sus subdirectorios.

~~~
$ grep -rin "HOLA mundo" *
~~~

* (r) Recursivamente.
* (i) Ignore case, no tiene en cuenta las mayúsculas y minúsculas.
* (n) Number, muestra el número de linea en el que se encuentra la cadena.
* (c) Muestra el número de lineas que coinciden.
* (v) Muestra las lineas que no coindicén con el patrón buscado.
* (o) Indica a grep que solo muestre la parte que coincide con el patrón.
* (H) Muestra el nombre del archivo con cada coincidencia.


## COMANDO WS

Sirve para contar lineas, palabras y caracteres de un archivo, su sintaxis es la siguiente:

~~~
$ wc -opciones hola.txt
~~~

Las cuales despliegan cuatro parámetros de salida que son:

~~~
1 4 23 hola.txt
~~~

- **Primer parámetro** Número de lineas.
- **Segundo parámetro** Número de palabras.
- **Tercer parámetro** Número de caracteres.

Ejemplos

La opción c muestra solamente el número de caracteres.

~~~
$ wc -c /etc/passwd
~~~

La opción w muestra solamente el número de palabras.

~~~
$ wc -w /etc/passwd
~~~

La opción l muestra solamente el número de lineas.

~~~
$ wc -l /etc/passwd
~~~

## Ver los procesos que se están ejecutando

Para ver los procesos que se están ejecutando en Linux

~~~
ps aux
~~~

Para ver los procesos referentes a un programa en concreto

~~~
ps aux | grep name_process
~~~

Para matar el proceso

~~~
kill -9 1986
~~~

Un dato a tener en cuenta al usar este método es que en caso de que haya más de una instancia de ese programa ejecutándose, se cerrarán todas.

~~~
killall nombre_proceso
~~~


## Tuberías (Pipes)

## Creación de Scripts

### Whereis, which

Se puede usar el comando whereis para saber la ubicación de un binario ejecutable en su sistema, también se puede usar el comando which.

Escribimos el siguiente comando para saber en cual ambiente de bash estamos trabajando.

~~~
which bash
~~~

Ahora creamos el script con extensión .sh con el siguiente comando:

~~~
nano crearCarpetas.sh
~~~

La estructura del escrito seguira el siguiente formato:

~~~
#!/bin/bash

echo "----Introducior la inicial de la carpetas a buscar---"

read var

echo "---Las coincidencias son---"
sudo ls | grep $var
~~~

Existen tres diferente formas de hacer correr un script

Forma 1

~~~
sh crearCarpetas.sh
~~~

Forma 2

~~~
source crearCarpetas.sh
~~~

Forma 3

~~~
./crearCarpetas.sh
~~~

Ojo para esta última forma hay que darle permisos de ejecución.

Para crear 

## Intalación de LAMP

Actualizar los repositorios. Luego 

~~~
$ sudo apt-get install apache2
~~~

Reiniciar el servidor apache

~~~
$ sudo /etc/init.d/apache2 restart
~~~

Ver los errores de Apache2

~~~
/var/log/apache2/error.log
~~~


